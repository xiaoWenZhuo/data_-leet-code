//给你一个正整数 n ，生成一个包含 1 到 n² 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 3
//输出：[[1,2,3],[8,9,4],[7,6,5]]
// 
//
// 示例 2： 
//
// 
//输入：n = 1
//输出：[[1]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= n <= 20 
// 
// 👍 658 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[][] generateMatrix(int n) {
        int[][] result = new int[n][n];
        int l=0,r=n-1,t=0,b=n-1;
        int start = 1 ,end = n*n;
        while (start<=end){
            for (int i = l ;i<=r;i++){
                result[t][i] = start++;
            }
            t++;
            for (int j=t;j<=b;j++){
                result[j][r] =start++;
            }
            r--;
            for (int i=r;i>=l;i--){
                result[b][i] = start++;
            }
            b--;
            for (int i = b;i>=t;i--){
                result[i][l] = start++;
            }
            l++;
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
