//给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。 
//
// 注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。 
//
// 
//
// 示例 1: 
//
// 
//输入: s = "anagram", t = "nagaram"
//输出: true
// 
//
// 示例 2: 
//
// 
//输入: s = "rat", t = "car"
//输出: false 
//
// 
//
// 提示: 
//
// 
// 1 <= s.length, t.length <= 5 * 10⁴ 
// s 和 t 仅包含小写字母 
// 
//
// 
//
// 进阶: 如果输入字符串包含 unicode 字符怎么办？你能否调整你的解法来应对这种情况？ 
// 👍 525 👎 0



//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    //哈希表简化思路
    //只存储字母 所以可以定义数组实现哈希表
    //使用 Char c -‘a’ 即可通过int[] arr = new int[26]来存储相关字母
    //之后遍历数组判断即可
    public boolean isAnagram(String s, String t) {
        char[] charS = s.toCharArray();
        char[] charT = t.toCharArray();
        HashMap<Character, Integer> mapS = new HashMap<Character, Integer>();
        for (char aChar : charS) {
            if(mapS.containsKey(aChar)){
                mapS.put(aChar, mapS.get(aChar) + 1);
            }else{
                mapS.put(aChar,1);
            }
        }
        HashMap<Character, Integer> mapT = new HashMap<Character, Integer>();
        for (char aChar : charT) {
            if(mapT.containsKey(aChar)){
                mapT.put(aChar, mapT.get(aChar) + 1);
            }else{
                mapT.put(aChar,1);
            }
        }
        HashMap<Character, Integer> resultMap = mapS.keySet().size() > mapT.keySet().size() ? mapS : mapT;
        Set<Character> characters = resultMap.keySet();
        for (Character character : characters) {
            if(!mapT.containsKey(character)|| !Objects.equals(mapT.get(character), mapS.get(character)))
            return false;
        }
        return true;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
