import java.util.ArrayList;
import java.util.List;

/**
/**
给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。 

 

 示例 1： 

 
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 

 示例 2： 

 
输入：nums = [0,1]
输出：[[0,1],[1,0]]
 

 示例 3： 

 
输入：nums = [1]
输出：[[1]]
 

 

 提示： 

 
 1 <= nums.length <= 6 
 -10 <= nums[i] <= 10 
 nums 中的所有整数 互不相同 
 
 Related Topics 数组 回溯 👍 1759 👎 0

*/



//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Integer[] nums1 = Arrays.stream(nums).boxed().toArray(Integer[]::new);
        process(nums,0,res);
        return res;
    }

    private void process(int[] nums, int i, List<List<Integer>> res) {
        if(i==nums.length){
            res.add(List.of(nums));
        }
        for (int j=i;j<nums.length;j++) {
            swap(nums,i,j);
            process(nums,i+1,res);
            swap(nums,i,j);
        }
    }

    public static void swap(Integer[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
