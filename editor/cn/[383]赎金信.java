//给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。 
//
// 如果可以，返回 true ；否则返回 false 。 
//
// magazine 中的每个字符只能在 ransomNote 中使用一次。 
//
// 
//
// 示例 1： 
//
// 
//输入：ransomNote = "a", magazine = "b"
//输出：false
// 
//
// 示例 2： 
//
// 
//输入：ransomNote = "aa", magazine = "ab"
//输出：false
// 
//
// 示例 3： 
//
// 
//输入：ransomNote = "aa", magazine = "aab"
//输出：true
// 
//
// 
//
// 提示： 
//
// 
// 1 <= ransomNote.length, magazine.length <= 10⁵ 
// ransomNote 和 magazine 由小写英文字母组成 
// 
// 👍 288 👎 0


import java.util.HashMap;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        //HashMap<Character, Integer> map1 = new HashMap<Character, Integer>();
        //HashMap<Character, Integer> map2 = new HashMap<Character, Integer>();
        //    for (char c : ransomNote.toCharArray()) {
        //        if(!map1.containsKey(c)){
        //            map1.put(c, 1);
        //        }else{
        //            map1.put(c, map1.get(c)+1);
        //        }
        //    }
        //    for (char c : magazine.toCharArray()) {
        //        if(!map2.containsKey(c)){
        //            map2.put(c,1);
        //        }else{
        //            map2.put(c,map2.get(c) + 1);
        //        }
        //    }
        //
        //    for (Character character : map1.keySet()) {
        //        System.out.println(character+"  "+map1.get(character) +" " +map2.get(character));
        //        if(!map2.containsKey(character)){
        //            return false;
        //        }else{
        //            if(map2.get(character)<map1.get(character)){
        //                return false;
        //            }
        //        }
        //    }
        //    return true;
        //}
        //记录杂志字符串出现的次数
        int[] arr = new int[26];
        int temp;
        for (int i = 0; i < magazine.length(); i++) {
            temp = magazine.charAt(i) - 'a';
            arr[temp]++;
        }
        for (int i = 0; i < ransomNote.length(); i++) {
            temp = ransomNote.charAt(i) - 'a';
            //对于金信中的每一个字符都在数组中查找
            //找到相应位减一，否则找不到返回false
            if (arr[temp] > 0) {
                arr[temp]--;
            } else {
                return false;
            }
        }
        return true;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
