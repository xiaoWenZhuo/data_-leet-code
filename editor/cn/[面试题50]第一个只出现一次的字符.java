//在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。 s 只包含小写字母。 
//
// 示例 1: 
//
// 
//输入：s = "abaccdeff"
//输出：'b'
// 
//
// 示例 2: 
//
// 
//输入：s = "" 
//输出：' '
// 
//
// 
//
// 限制： 
//
// 0 <= s 的长度 <= 50000 
// 👍 189 👎 0



//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public char firstUniqChar(String s) {
        int[] arr = new int[128];
        //0-127
        LinkedList<Character> queue = new LinkedList<>();
        char[] chars = s.toCharArray();
        for (char aChar : chars) {
            arr[aChar]++;
            queue.add(aChar);
            while (!queue.isEmpty()&&arr[queue.peek()]>1){
                queue.poll();
            }
        }
        return queue.isEmpty()?' ':queue.peek();
    }
}
//leetcode submit region end(Prohibit modification and deletion)
