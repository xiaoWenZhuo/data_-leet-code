package util;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: util
 * @author: 肖
 * @date: 2022/1/28 10:35
 * @Description: 树节点
 * @since JDK 11
 */

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}