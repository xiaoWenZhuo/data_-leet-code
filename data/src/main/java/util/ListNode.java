package util;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: util
 * @author: 肖
 * @date: 2022/1/28 10:33
 * @Description:
 * @since JDK 11
 */

public class ListNode {
    public int val;
    public ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
