package util;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: util
 * @author: 肖
 * @date: 2022/1/28 10:18
 * @Description: 树节点
 * @since JDK 11
 */
public  class Node {
    public int value;
    public Node left;
    public Node right;
}