package util;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: util
 * @author: 肖
 * @date: 2022/1/25 10:25
 * @Description: 封装交换
 * @since JDK 11
 */

public class SwapUtil {
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
