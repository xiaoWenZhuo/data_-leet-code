package graph;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: graph
 * @author: 肖
 * @date: 2022/1/29 20:31
 * @Description: 宽度优先遍历
 * @since JDK 11
 */

public class BFS {
    public static void bfs(NodeG node){
        if(node==null){
            return;
        }
        Queue<NodeG> queue = new LinkedList<>();
        HashSet<NodeG> set = new HashSet<>();
        queue.add(node);
        set.add(node);
        while(!queue.isEmpty()){
            NodeG cur = queue.poll();
//          定制 加操作
            System.out.println(cur.value);
            for (NodeG next: cur.nexts){
                if(!set.contains(next)){
                    set.add(next);
                    queue.add(next);
                }
            }
        }
    }

}
