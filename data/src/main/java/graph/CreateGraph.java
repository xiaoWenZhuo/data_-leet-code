package graph;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: graph
 * @author: 肖
 * @date: 2022/1/29 20:13
 * @Description: 二维数组构建图
 * @since JDK 11
 */

public class CreateGraph {
    public static Graph createGraph(Integer[][] matrix) {
        Graph graph = new Graph();
        for (int i = 0; i < matrix.length; i++) {
//          获取数据值
            Integer from = matrix[i][0];
            Integer to = matrix[i][1];
            Integer weight = matrix[i][2];
            if(!graph.nodes.containsKey(from)){
                graph.nodes.put(from,new NodeG(from));
            }
            if(!graph.nodes.containsKey(to)){
                graph.nodes.put(to,new NodeG(to));
            }
//          获取存入的点
            NodeG fromNode = graph.nodes.get(from);
            NodeG toNode = graph.nodes.get(to);
            Edge newEdge = new Edge(weight,fromNode,toNode);
            fromNode.nexts.add(toNode);
            fromNode.out++;
            toNode.in++;
            fromNode.edges.add(newEdge);
            graph.edges.add(newEdge);
        }
        return graph;
    }
}
