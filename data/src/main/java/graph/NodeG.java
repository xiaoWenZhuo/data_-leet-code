package graph;

import java.util.ArrayList;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: graph
 * @author: 肖
 * @date: 2022/1/29 20:03
 * @Description: 图 结点
 * @since JDK 11
 */

public class NodeG {
    public int value;
//    入度
    public int in;
//    出度
    public int out;
//    直接邻居点 只关心出（发散边）
    public ArrayList<NodeG> nexts;
//    属于边 也是发散边
    public ArrayList<Edge> edges;

    public NodeG(int value) {
        this.value = value;
        in =0;
        out =0;
        nexts = new ArrayList<>();
        edges = new ArrayList<>();
    }
}
