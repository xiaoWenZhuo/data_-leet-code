package graph;

import util.Node;

import java.util.HashSet;
import java.util.Stack;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: graph
 * @author: 肖
 * @date: 2022/1/29 20:40
 * @Description: 深度优先遍历
 * @since JDK 11
 */

public class DFS {
    public static void dfs(NodeG node){
        if(node==null){
            return;
        }
        HashSet<NodeG> set = new HashSet<NodeG>();
        Stack<NodeG> stack = new Stack<NodeG>();
        stack.add(node);
        set.add(node);
        System.out.println(node.value);
        while (!stack.isEmpty()){
            NodeG cur =stack.pop();
            for (NodeG next: cur.nexts){
                if(!set.contains(next)){
                    stack.push(cur);
                    stack.push(next);
                    set.add(next);
                    System.out.println(next.value);
                    break;
                }
            }
        }
    }

}
