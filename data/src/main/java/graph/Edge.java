package graph;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: graph
 * @author: 肖
 * @date: 2022/1/29 20:02
 * @Description: 边集
 * @since JDK 11
 */

public class Edge {
//    权值/距离
    public int weight;
    public NodeG from;
    public NodeG to;
    public Edge(int weighet, NodeG from, NodeG to) {
        this.weight = weighet;
        this.from = from;
        this.to = to;
    }

}
