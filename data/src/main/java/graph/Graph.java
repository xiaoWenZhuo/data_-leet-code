package graph;



import java.util.HashMap;
import java.util.HashSet;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: graph
 * @author: 肖
 * @date: 2022/1/29 20:01
 * @Description: 图
 * @since JDK 11
 */

public class Graph {
    public HashMap<Integer,NodeG> nodes;
    public HashSet<Edge> edges;

    public Graph() {
       nodes = new HashMap<>();
       edges = new HashSet<>();
    }
}
