package sort;


import java.util.PriorityQueue;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: sort
 * @author: 肖
 * @date: 2022/1/25 17:59
 * @Description: 小根堆 几乎有序
 * @since JDK 11
 */

public class SortArrayDistance {
    public static void SortArrayDistance(int[] arr,int k) {
//        java自带的优先队列 默认是小顶堆 每次扩容变大一倍
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int index =0 ;
        for(;index<Math.min(arr.length, k);index++){
            heap.add(arr[index]);
        }
        int i = 0 ;
        for(;index<arr.length;i++,index++){
            heap.add(arr[index]);
            arr[i]=heap.poll();
        }
        while (!heap.isEmpty()){
            arr[i++] = heap.poll();
        }
    }
}
