package sort;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: sort
 * @author: 肖
 * @date: 2022/1/20 18:56
 * @Description: 冒泡排序
 * @since JDK 11
 */

public class BubbleSort {
    void BubbleSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int e = arr.length - 1; e > 0; e--) {
            for (int i = 0; i < e; i++) {
                if (arr[i] > arr[i + 1]) {
                    swap(arr, i, i + 1);
                }
            }
        }
    }

    void swap(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

}
