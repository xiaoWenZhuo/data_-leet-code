package sort;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: sort
 * @author: 肖
 * @date: 2022/1/20 17:40
 * @Description: 选择排序
 * @since JDK 11
 */

public class SelectSort {
    void SelectSort(int[] arr) {
        //不做处理的条件
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i; j < arr.length - 1; j++) {
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;
                swap(arr, i, minIndex);
            }
        }
    }

    void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}

