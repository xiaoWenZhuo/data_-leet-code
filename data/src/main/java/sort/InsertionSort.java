package sort;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: sort
 * @author: 肖
 * @date: 2022/1/21 8:53
 * @Description: 插入排序
 * @since JDK 11
 */

public class InsertionSort {
    void InsertionSort(int[] arr){
        if(arr==null||arr.length<2){
            return ;
        }
        for (int i = 1; i < arr.length;i++){
            for(int j=i-1;j>=0&&arr[j]>arr[j+1];j--){
                swap(arr,j,j+1);
            }
        }
    }
    void swap(int[] arr,int i , int j){
        int temp = arr[i];
        arr[i] = arr [j];
        arr[j] = temp;
    }


}
