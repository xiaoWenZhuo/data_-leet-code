package tree;

import util.Node;

import java.util.LinkedList;

/**
 * @BelongsProject: leetcode
 * @BelongsPackage: tree
 * @author: 肖
 * @date: 2022/1/28 10:16
 * @Description: 完全二叉树
 * @since JDK 11
 */

public class TestCBT {
    /*
     * 宽度优先遍历 用队列
     * 1>有右无左 false
     * 2>在满足1>下 若左右不全 则接下来的必全为叶子节点
     * */
    public static boolean isCBT(Node head) {
        if (head == null) {
            return true;
        }
        LinkedList<Node> queue = new LinkedList<>();
//        1>
        boolean leaf = false;
        Node l = null;
        Node r = null;
        queue.add(head);
        while (!queue.isEmpty()) {
            head = queue.pop();
            l = head.left;
            r = head.right;
            if ((leaf && (l != null || r != null)) || (l == null && r != null)) {
                return false;
            }
            if (l != null) {
                queue.add(l);
            }
            if (r != null) {
                queue.add(r);
            }
            if (l == null || r == null) {
                leaf = true;
            }
        }
        return true;
    }

}
