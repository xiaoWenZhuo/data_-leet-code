package others;

public class NQueens {
    public static int num1(int n){
        if(n<1){
            return 0;
        }
//        n行皇后放到了第几列
        int [] record = new int[n];
        return process(0,record,n);
    }

    public static int process(int i,int[] record,int n){
        if(i==n){
            return 1;
        }
        int res = 0;
        for (int j=0 ;j <n;j++){
//            i行放j列
            if(isVaild(record,i,j)){
                record[i] = j;
                res+=process(i+1,record,n);
            }
        }
        return res;
    }

    private static boolean isVaild(int[] record, int i, int j) {
        for (int k = 0 ; k<i ; k++){
            if(j==record[k]||Math.abs(record[k]-j)==Math.abs(i-k)){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int i = num1(4);
        System.out.println(i);
    }
}
